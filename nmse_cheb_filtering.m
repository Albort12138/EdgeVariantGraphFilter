% This script generate the Chebyshev and J-Chebyshev plots for the CAMSAP
% paper.

clear all;
close all;
clc;


%% Graph Generation

G = gsp_community(256);
paramplot.show_edges = 1;
gsp_plot_graph(G,paramplot);

N = G. N; % number of nodes
G = gsp_compute_fourier_basis(G);

% Laplacian
Ld = full(G.L);

% degree matrix
D= diag(diag(Ld));

% normalized Laplacian
L_norm = D^(-1/2)*Ld*D^(-1/2);

% eigendecomposition
[U, Lam] = eig(L_norm);

% The graph signal is all 1 in the Graph Fourier domain
xf = ones(N,1);
% graph signal in the vertex domain
x = U*xf;

%% Design parameters - Low pass

% Full order
Kfull = 22; % the LS explodes for higher order

% universal grid
lam = linspace(0,max(diag(Lam)),50);

% cut-off
lc = 1;

% desired step - response
dresp = @(lambda)double(lambda<lc);

% evaluate the desired response for the particular graph
step_resp = dresp(diag(Lam));

nmse_stepCh = zeros(Kfull,1); % NMSE Chebyshev
%nmse_stepJCh = zeros(Kfull,1); % NMSE J-Chebyshev
nmse_stepLS = zeros(Kfull,1); % NMSE LS

G.L = L_norm;
G.U = U;
G.e = diag(Lam);
G = gsp_estimate_lmax(G);

for iK = 1 : Kfull
    K = iK;
    [ch,jch] = jackson_cheby_poly_coefficients(0,lc,[0,max(diag(Lam))],K);
    
    % step-poly
    pol_ls = polyfit(lam, dresp(lam), K);
    pol_ls = wrev(pol_ls); % put in increasing order
    
    % Chebyshev
    r_ch = gsp_cheby_op(G, ch, x);
    nmse_stepCh(iK) = norm(U'*r_ch-step_resp).^2./norm(step_resp).^2;
    
    % Jackson Chebyshev
    %r_jch = gsp_cheby_op(G, jch, x);
    %nmse_stepJCh(iK) = norm(U'*r_jch-step_resp).^2./norm(step_resp).^2;
    
    H_ls = zeros(N,N);
    % LS
    for iK2 = 0 : iK
        temp = pol_ls(iK2+1)*L_norm^(iK2);
        H_ls = H_ls + temp;
    end
    r_ls = H_ls*x;
    
    nmse_stepLS(iK) = norm(U'*r_ls-step_resp).^2./norm(step_resp).^2;
    clear temp;

    
end
clear iK;

figure;
plot(1:Kfull,nmse_stepCh,'-o')
hold on;
%plot(1:Kfull,nmse_stepJCh,'-s')
plot(1:Kfull,nmse_stepLS,'-v')
legend('Chebyshev','Jackson-Chebyshev','LS')
legend('Chebyshev','LS')


%% Design hear kernel

% desired heat kernel - response
gamma = 3; % weight parameter
% drespKer = @(lambda)(1./(1+gamma*lambda));
drespKer = @(lambda)(exp(-gamma*(lambda - 0.75).^2));
ker_resp = drespKer(diag(Lam));

clear ch;
clear pol_ls;

nmse_kerCh = zeros(Kfull,1); % NMSE Chebyshev
nmse_kerLS = zeros(Kfull,1); % NMSE LS


for iK = 1 : Kfull
    K = iK;
    
    % Chebyshev
    ch = gsp_cheby_coeff(G, drespKer,K);
    r_ch = gsp_cheby_op(G, ch, x);
    nmse_kerCh(iK) = norm(U'*r_ch-ker_resp).^2./norm(ker_resp).^2;

    % LS
    pol_ls = polyfit(lam, drespKer(lam), K);
    pol_ls = wrev(pol_ls); % put in increasing order
    
    H_ls = zeros(N,N);
    % LS
    for iK2 = 0 : iK
        temp = pol_ls(iK2+1)*L_norm^(iK2);
        H_ls = H_ls + temp;
    end
    r_ls = H_ls*x;
    
    nmse_stepLS(iK) = norm(U'*r_ls-ker_resp).^2./norm(ker_resp).^2;
    clear temp;
    
end

figure;
semilogy(1:Kfull,nmse_kerCh,'-o')
hold on;
semilogy(1:Kfull,nmse_stepLS,'-v')
legend('Chebyshev','LS')

